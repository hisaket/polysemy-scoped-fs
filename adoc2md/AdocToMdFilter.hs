import Text.Pandoc.Definition
import Text.Pandoc.JSON
import Text.Pandoc.Shared

addTitleAsHeader :: Pandoc -> Pandoc
addTitleAsHeader (Pandoc meta blocks) =
    Pandoc meta $ Header 1 nullAttr (docTitle meta) : blocks

main :: IO ()
main = toJSONFilter (addTitleAsHeader . headerShift 1)