#!/bin/bash

asciidoctor --backend docbook5 README.adoc --out-file - | pandoc -f docbook -t gfm --filter adoc2md/AdocToMdFilter.hs > README.md
asciidoctor --backend docbook5 ChangeLog.adoc --out-file - | pandoc -f docbook -t gfm --filter adoc2md/AdocToMdFilter.hs > ChangeLog.md