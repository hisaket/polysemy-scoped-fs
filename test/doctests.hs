module Main where

import Test.DocTest

main :: IO ()
main = doctest $
    map ("-X" <>) exts
        <> ["-fplugin=Polysemy.Plugin"]
        <> ["-isrc", "src/Polysemy/FS/Scoped/Text.hs"]
        <> ["-isrc", "src/Polysemy/ScopedReader.hs"]
        <> ["-isrc", "src/Polysemy/FS/Scoped/Oneshot.hs"]

-- TODO: fetch automatically from package.yaml
exts :: [String]
exts =
    [   "DataKinds"
    ,   "FlexibleContexts"
    ,   "GADTs"
    ,   "LambdaCase"
    ,   "PolyKinds"
    ,   "RankNTypes"
    ,   "ScopedTypeVariables"
    ,   "TypeApplications"
    ,   "TypeOperators"
    ,   "TypeFamilies"
    ,   "UnicodeSyntax"
    ,   "BlockArguments"
    ,   "TemplateHaskell"
    ,   "QuasiQuotes"
    ,   "OverloadedStrings"
    ]