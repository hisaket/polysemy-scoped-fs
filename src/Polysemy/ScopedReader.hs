{-|
Copyright   : (c) Hisaket VioletRed, 2022
License     : AGPL-3.0-or-later
Maintainer  : hisaket@outlook.jp
Stability   : experimental

This module provides a scoped-reader manner.

In contrast to a normal 'Reader' effect, this manner makes connection of parameter type and effect explicit.

Example\:

>>> import Polysemy ( interpret, runM, embed )
>>> import Polysemy.Output ( Output (Output), output, runOutputSem )

>>> :{
runDebug :: Member (Output String) r => String -> InterpreterFor (ScopedReader String (Output String)) r
runDebug = runScopedReader \i -> interpret \(Output o) -> output $ "[" <> i <> "] " <> o
:}

>>> :{
runM $ runOutputSem (embed . putStrLn) $ runDebug "root" do
    scopedReader $ output "test message 0"
    scopedLocal (<> ".scope-A") do
        scopedReader $ output "test message 1"
        scopedReader $ output "test message 2"
        scopedLocal (<> ".scope-B") do
            scopedReader $ output "test message 3"
:}
[root] test message 0
[root.scope-A] test message 1
[root.scope-A] test message 2
[root.scope-A.scope-B] test message 3

-}

module Polysemy.ScopedReader where

import Polysemy ( Member, Sem, raise, InterpreterFor )
import Polysemy.Scoped.Parameterized.Internal ( interpretH' )
import Polysemy.Internal ( liftSem, send )
import Polysemy.Internal.Union
    ( Member
    , injWeaving
    , ElemOf(Here)
    , Union(Union)
    , Weaving(Weaving)
    )

scopedLocal
    ::  Member (ScopedReader i effect) r
    =>  (i -> i) -> Sem r a -> Sem r a
scopedLocal f = send . ScopedLocal f

scopedReader :: Member (ScopedReader i effect) r => Sem (effect ': r) a -> Sem r a
scopedReader =
    interpretH' \(Weaving e s wv ex ins) ->
        liftSem $ injWeaving $ Weaving (ScopedReader e) s (scopedReader . wv) ex ins

runScopedReader :: (i -> InterpreterFor effect r) -> i -> Sem (ScopedReader i effect ': r) a -> Sem r a
runScopedReader int i =
    interpretH' \(Weaving e s wv ex ins) ->
        case e of
            ScopedReader e' ->
                int i
                    $ liftSem $ Union Here
                    $ Weaving e' s (raise . runScopedReader int i . wv) ex ins
            ScopedLocal f inner ->
                ex <$> runScopedReader int (f i) (wv $ inner <$ s)

data ScopedReader i effect m a where
    ScopedReader :: effect m a -> ScopedReader i effect m a
    ScopedLocal :: (i -> i) -> m a -> ScopedReader i effect m a
