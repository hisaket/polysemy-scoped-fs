{-|
Copyright   : (c) Hisaket VioletRed, 2022
License     : AGPL-3.0-or-later
Maintainer  : hisaket@outlook.jp
Stability   : experimental
-}

module Polysemy.Scoped.Generic where
import Polysemy (Member, Sem, Members, transform, InterpreterFor)
import Polysemy.Bundle (Bundle, injBundle)
import Polysemy.Internal.Sing (KnownList)
import Polysemy.Scoped.Internal
    ( bundle, unbundle, weakenBundle )
import Data.Functor ((<&>))

class GenericScopedP s where
    -- | Generic safely (without leaking a resource value) folding function on the 'ScopedP' data structure.
    foldScopedP
        ::  (   ((∀y. (eff' m' a' -> y) -> y) -> s p resource eff' m' a')
            ->  ((∀y. (resource' -> eff' m' a' -> y) -> y) -> s p resource' eff' m' a')
            ->  eff m a -> x
            )
        ->  (   (   (   ∀y
                    .   (   (resource' -> resource)
                        ->  ((∀q e n z. e n z -> s q resource' e n z) -> m a -> m' a')
                        ->  p'
                        ->  y
                        )
                    ->  y
                    )
                ->  s p' resource' eff' m' a'
                )
            ->  p -> (resource -> m a) -> x
            )
        ->  s p resource eff m a
        ->  x

    mapResource
        ::  (resource -> resource)
        ->  s p resource eff m a
        ->  s p resource eff m a
    mapResource f s =
        foldScopedP
            (\_ _ _ -> s)
            (\inScopeSetter p inner -> inScopeSetter \set -> set f (const id) p)
            s

    mapEff :: (eff0 m a -> eff1 m a) -> s p resource eff0 m a -> s p resource eff1 m a
    mapEff f =
        foldScopedP
            (\runSetter _ eff -> runSetter \set -> set $ f eff)
            (\inScopeSetter p inner -> inScopeSetter \set -> set id (const id) p)

    traverseEff
        ::  Applicative f
        =>  (eff0 m a -> f (eff1 m a))
        ->  s p resource eff0 m a
        ->  f (s p resource eff1 m a)
    traverseEff f =
        foldScopedP
            (\setterRun _ eff -> f eff <&> \eff' -> setterRun \set -> set eff')
            (\setterInScope p inner -> pure $ setterInScope \set -> set id (const id) p)

injScopedP
    ::  ∀eff es s p resource r a
    .   (Member (s p resource (Bundle es)) r, Member eff es, GenericScopedP s)
    =>  Sem (s p resource eff ': r) a -> Sem r a
injScopedP = transformScopedP injBundle

unbundleScopedP
    ::  (Member (s p resource eff) r, GenericScopedP s)
    =>  Sem (s p resource (Bundle '[eff]) ': r) a -> Sem r a
unbundleScopedP = transformScopedP unbundle

weakenScopedP
    ::  ∀es0 es1 s p resource r a
    .   ( GenericScopedP s
        , Member (s p resource (Bundle es1)) r
        , Members es0 es1
        , KnownList es0
        )
    =>  Sem (s p resource (Bundle es0) ': r) a -> Sem r a
weakenScopedP = transformScopedP weakenBundle

transformScopedP
    ::  (Member (s p resource e1) r, GenericScopedP s)
    =>  (∀rInitial x. e0 (Sem rInitial) x -> e1 (Sem rInitial) x)
    ->  Sem (s p resource e0 ': r) a
    ->  Sem r a
transformScopedP f = transform $ mapEff f