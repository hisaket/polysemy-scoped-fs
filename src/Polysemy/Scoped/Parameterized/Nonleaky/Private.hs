{-
This module contains the modified source code from polysemy-conc-0.7.0.0 package
under 'BSD-2-Clause-Patent'. So that part is only licensed under
'AGPL-3.0-or-later AND BSD-2-Clause-Patent', other part is licensed under
'AGPL-3.0-or-later'.
-}

-- -----BEGIN AGPL-3.0-or-later PART-----
-- Copyright (C) 2022 Hisaket VioletRed

{-|
License     : See the header comment of the source file of this module.
Maintainer  : hisaket@outlook.jp
Stability   : experimental
Portability : POSIX
-}

{-# LANGUAGE PatternSynonyms #-}

module Polysemy.Scoped.Parameterized.Nonleaky.Private
    ( NonleakyScopedP
    , subsumeNonleaky, runNonleakyScopedP
    , pattern NonleakyInScope, pattern NonleakyRun, run
    ) where

import Control.Category ( (>>>) )
import Polysemy ( Member, Sem, transform, InterpreterFor, raise )
import Polysemy.Scoped.Parameterized.Internal
    ( ScopedP (Run, InScope), interpretH' )
import Polysemy.Internal.Union ( Weaving(Weaving), injWeaving )
import Polysemy.Internal ( liftSem, send )
import Polysemy.Scoped.Generic (GenericScopedP (mapEff, traverseEff, foldScopedP, mapResource))


subsumeNonleaky
    ::  Member (NonleakyScopedP p resource eff) r
    =>  (∀resource'. Sem (ScopedP p resource' eff ': r) a)
    ->  Sem r a
subsumeNonleaky = transform \case
    Run resource act -> UnsafeRun resource act
    InScope p inner -> UnsafeInScope p inner


pattern NonleakyInScope :: p -> (resource -> m a) -> NonleakyScopedP p resource eff m a
pattern NonleakyInScope param inner <- UnsafeInScope param inner

pattern NonleakyRun :: eff m a -> NonleakyScopedP p resource eff m a
pattern NonleakyRun act <- UnsafeRun _resource act

run :: resource -> eff m a -> NonleakyScopedP p resource eff m a
run = UnsafeRun


instance GenericScopedP NonleakyScopedP where
    foldScopedP onRun onInScope = \case
        UnsafeRun resource act ->
            onRun
                (\withRun -> withRun $ \act' -> UnsafeRun resource act')
                (\withRun -> withRun $ \resource' act' -> UnsafeRun resource' act')
                act
        UnsafeInScope param inner ->
            onInScope
                ( \withInScope -> withInScope $ \f g param' ->
                    UnsafeInScope param' \resource -> g (UnsafeRun resource) $ inner $ f resource
                )
                param inner

    mapResource f s = case s of
        UnsafeRun _ _ -> s
        UnsafeInScope param inner -> UnsafeInScope param (inner . f)

    mapEff f = \case
        UnsafeRun resource act -> UnsafeRun resource $ f act
        UnsafeInScope param inner -> UnsafeInScope param inner

    traverseEff f = \case
        UnsafeRun resource act -> UnsafeRun resource <$> f act
        UnsafeInScope param inner -> pure $ UnsafeInScope param inner

-- -----END AGPL-3.0-or-later PART-----

-- -----BEGIN AGPL-3.0-or-later AND BSD-2-Clause-Patent PART-----
{-
Copyright (c) 2020 Torsten Schmits
Copyright (c) 2022 Hisaket VioletRed
-}

{-
The below functions are modified (or unmodified) version from modules
'Polysemy.Conc.Effect.Scoped' and 'Polysemy.Conc.Interpreter.Scoped' in
polysemy-conc-0.7.0.0 package.
-}

data NonleakyScopedP p resource eff m a where
    UnsafeRun :: resource -> eff m a -> NonleakyScopedP p resource eff m a
    UnsafeInScope :: p -> (resource -> m a) -> NonleakyScopedP p resource eff m a

runNonleakyScopedP
    ::  ∀p resource eff r
    .   (∀x. p -> (resource -> Sem r x) -> Sem r x)
    ->  (resource -> InterpreterFor eff r)
    ->  InterpreterFor (NonleakyScopedP p resource eff) r
runNonleakyScopedP withResource scopedInterpreter = run
  where
    run :: InterpreterFor (NonleakyScopedP p resource eff) r
    run =
        interpretH' \(Weaving eff s wv ex ins) -> case eff of
            UnsafeRun resource act ->
                scopedInterpreter resource $ liftSem $ injWeaving $ Weaving act s (raise . run . wv) ex ins
            UnsafeInScope param main ->
                ex <$> withResource param \resource -> run $ wv $ main resource <$ s


{-
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
  disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
  disclaimer in the documentation and/or other materials provided with the distribution.

Subject to the terms and conditions of this license, each copyright holder and contributor hereby grants to those
receiving rights under this license a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable (except
for failure to satisfy the conditions of this license) patent license to make, have made, use, offer to sell, sell,
import, and otherwise transfer this software, where such license applies only to those patent claims, already acquired
or hereafter acquired, licensable by such copyright holder or contributor that are necessarily infringed by:

  (a) their Contribution(s) (the licensed copyrights of copyright holders and non-copyrightable additions of
  contributors, in source or binary form) alone; or
  (b) combination of their Contribution(s) with the work of authorship to which such Contribution(s) was added by such
  copyright holder or contributor, if, at the time the Contribution is added, such addition causes such combination to
  be necessarily infringed. The patent license shall not apply to any other combinations which include the Contribution.

Except as expressly stated above, no rights or licenses from any copyright holder or contributor is granted under this
license, whether expressly, by implication, estoppel or otherwise.

DISCLAIMER

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-}
-- -----END AGPL-3.0-or-later AND BSD-2-Clause-Patent PART-----