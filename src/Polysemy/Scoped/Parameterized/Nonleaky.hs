{-|
Copyright   : (c) Hisaket VioletRed, 2022
License     : AGPL-3.0-or-later
Maintainer  : hisaket@outlook.jp
Stability   : experimental
-}

module Polysemy.Scoped.Parameterized.Nonleaky
    ( module Polysemy.Scoped.Parameterized.Nonleaky
    , Private.NonleakyScopedP, Private.subsumeNonleaky, Private.runNonleakyScopedP
    ) where

import qualified Polysemy.Scoped.Parameterized.Nonleaky.Private as Private

import Polysemy (raiseUnder, Member, Sem)
import Polysemy.Scoped.Parameterized (ScopedP)

rewriteNonleaky
    ::  (∀resource'. Sem (ScopedP p resource' eff ': r) a)
    ->  Sem (Private.NonleakyScopedP p resource eff ': r) a
rewriteNonleaky m = Private.subsumeNonleaky $ raiseUnder m
