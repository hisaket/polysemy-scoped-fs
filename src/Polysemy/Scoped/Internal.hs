{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use camelCase" #-}
{-# LANGUAGE EmptyCase #-}
module Polysemy.Scoped.Internal where

import Polysemy.Bundle (Bundle (Bundle))
import Polysemy.Internal.Union
    (Weaving(Weaving), ElemOf (Here, There), Union (Union), hoist, decomp, membership)
import Polysemy.Internal.Sing (KnownList (singList), SList (SEnd, SCons))
import Polysemy.Internal
    ( Sem (Sem, runSem), hoistSem, InterpreterFor, Members )

-- | Bundle all effects of 'Sem' into a single 'Bundle'.
bundle :: Sem es a -> Sem (Bundle es ': r) a
bundle =
    hoistSem \(Union w (Weaving e s wv ex ins)) ->
        Union Here $ Weaving (Bundle w e) s (bundle . wv) ex ins


subsumeBundle_id :: Sem (Bundle r ': r) a -> Sem r a
subsumeBundle_id = subsumeBundleUsing id

subsumeBundleUsing :: (∀e. ElemOf e es -> ElemOf e r) -> Sem (Bundle es ': r) a -> Sem r a
subsumeBundleUsing i = hoistSem $ \u -> hoist (subsumeBundleUsing i) $ case decomp u of
    Right (Weaving (Bundle pr e) s wv ex ins) ->
        Union (i pr) (Weaving e s wv ex ins)
    Left g -> g


unbundle :: Bundle '[e] m a -> e m a
unbundle = \case
    Bundle Here e -> e
    Bundle (There es) e -> case es of

weakenBundle :: (Members es r, KnownList es) => Bundle es m a -> Bundle r m a
weakenBundle (Bundle pr e) = Bundle (weakenMembership pr) e


weakenMembership :: (Members es r, KnownList es) => ElemOf e es -> ElemOf e r
weakenMembership = weakenMembership' singList

weakenMembership' :: Members es r => SList es -> ElemOf e es -> ElemOf e r
weakenMembership' l pr = case l of
    SEnd -> case pr of
    SCons xs -> case pr of
        Here -> membership
        There pr' -> weakenMembership' xs pr'