{-|
Copyright   : (c) Hisaket VioletRed, 2022
License     : AGPL-3.0-or-later
Maintainer  : hisaket@outlook.jp
Stability   : experimental

Unscoped file access and interop with polysemy-fs package.
-}

{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use camelCase" #-}
{-# LANGUAGE PartialTypeSignatures #-}

module Polysemy.FS.Scoped.Oneshot where

import Polysemy ( Member, Sem, Members, interpret, makeSem, run )
import Polysemy.FS.Scoped
    ( ScopedFile
    , AccessMode (ReadAccess, AppendAccess, WriteAccess)
    , Format (TextFormat, BytesFormat)
    , Mode
    , scopedFile_single
    )
import qualified Polysemy.SequentialAccess.Text as SAT
import qualified Polysemy.SequentialAccess.ByteString as SAB
import qualified Polysemy.SequentialAccess as SA
import Polysemy.Path
    ( toFilePath
    , parseSomeFile
    , Path
    , Abs
    , File
    , PathException
    , Rel
    , SomeBase
    )
import Polysemy.Error ( Error )
import Control.Category ( (>>>) )
import qualified Data.ByteString as BS
import Path ( SomeBase (..) )
import Prelude hiding ( readFile, appendFile )
import qualified Polysemy.FS.Scoped.Text as ST
import qualified Polysemy.FS.Scoped.ByteString as SB
import Polysemy.FS
    ( FSRead (ReadFileBS, ReadFileUtf8)
    , FSWrite (WriteFileBS, WriteFileUtf8)
    )
import Data.Either.Extra (fromRight')
import Control.Monad ((>=>))
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE


data ReadFile b i m a where
    ReadFile :: Path b File -> ReadFile b i m i
makeSem ''ReadFile

data OverwriteFile b o m a where
    OverwriteFile :: Path b File -> o -> OverwriteFile b o m ()
makeSem ''OverwriteFile

data AppendFile b o m a where
    AppendFile :: Path b File -> o -> AppendFile b o m ()
makeSem ''AppendFile


readFileToScoped
    ::  Member (ScopedFile (Mode fmt ReadAccess) '[SA.Read SA.ToEnd i] b handle) r
    =>  Sem (ReadFile b i ': r) a -> Sem r a
readFileToScoped =
    interpret \(ReadFile path) ->
        scopedFile_single path $ SA.read SA.ToEnd

overwriteFileToScoped
    ::  Member (ScopedFile (Mode fmt WriteAccess) '[SA.Overwrite o] b handle) r
    =>  Sem (OverwriteFile b o ': r) a -> Sem r a
overwriteFileToScoped =
    interpret \(OverwriteFile path o) ->
        scopedFile_single path $ SA.overwrite o

overwriteFileToScopedExtend
    ::  Member (ScopedFile (Mode fmt WriteAccess) '[SA.Extend o] b handle) r
    =>  Sem (OverwriteFile b o ': r) a -> Sem r a
overwriteFileToScopedExtend =
    interpret \(OverwriteFile path o) ->
        scopedFile_single path $ SA.extend o

appendFileToScoped
    ::  Member (ScopedFile (Mode fmt AppendAccess) '[SA.Append o] b handle) r
    =>  Sem (AppendFile b o ': r) a -> Sem r a
appendFileToScoped =
    interpret \(AppendFile path o) ->
        scopedFile_single path $ SA.append o


readFileBsToUtf8 :: Member (ReadFile b T.Text) r => Sem (ReadFile b BS.ByteString ': r) a -> Sem r a
readFileBsToUtf8 = interpret \(ReadFile path) -> TE.encodeUtf8 <$> readFile path

overwriteFileUtf8ToBs :: Member (OverwriteFile b BS.ByteString) r => Sem (OverwriteFile b T.Text ': r) a -> Sem r a
overwriteFileUtf8ToBs = interpret \(OverwriteFile path text) -> overwriteFile path $ TE.encodeUtf8 text

appendFileUtf8ToBs :: Member (AppendFile b BS.ByteString) r => Sem (AppendFile b T.Text ': r) a -> Sem r a
appendFileUtf8ToBs = interpret \(AppendFile path text) -> appendFile path $ TE.encodeUtf8 text


{- | Example:

>>> import Polysemy.Resource ( resourceToIO )
>>> import qualified Polysemy.FS.Scoped.Text as ST
>>> import qualified Polysemy.FS.Scoped.ByteString as SB
>>> import Polysemy.Scoped.Parameterized ( weakenScopedP )
>>> import Polysemy.FS.Scoped ( rewriteScopedFile, transformerToRewriter )
>>> import Polysemy ( runFinal, embedToFinal, subsume_ )
>>> import Polysemy.Error ( errorToIOFinal )
>>> import Data.ByteString as BS
>>> import Data.Text as T
>>> import Path ( Abs, Rel, PathException )
>>> :{
    fsReadToIO :: Sem '[FSRead] a -> IO (Either PathException a)
    fsReadToIO m =
        runFinal $ embedToFinal $ resourceToIO $ errorToIOFinal
            $   ST.readAccessToIO $ rewriteScopedFile (transformerToRewriter $ weakenScopedP @'[SAT.ReadToEnd] @_ @_ @(Path Abs _))
            $   ST.readAccessToIO $ rewriteScopedFile (transformerToRewriter $ weakenScopedP @'[SAT.ReadToEnd] @_ @_ @(Path Rel _))
            $   SB.readAccessToIO $ rewriteScopedFile (transformerToRewriter $ weakenScopedP @'[SAB.ReadToEnd] @_ @_ @(Path Abs _))
            $   SB.readAccessToIO $ rewriteScopedFile (transformerToRewriter $ weakenScopedP @'[SAB.ReadToEnd] @_ @_ @(Path Rel _))
            $   readFileToScoped @_ @BS.ByteString @Abs
            $   readFileToScoped @_ @BS.ByteString @Rel
            $   readFileToScoped @_ @T.Text @Abs
            $   readFileToScoped @_ @T.Text @Rel
            $   fsReadToReadFile
            $   subsume_ m
:}

-}

fsReadToReadFile
    ::  ∀handle0 handle1 handle2 handle3 r a
    .   Members
            '[  ReadFile Abs BS.ByteString
            ,   ReadFile Rel BS.ByteString
            ,   ReadFile Abs T.Text
            ,   ReadFile Rel T.Text
            ,   Error PathException
            ] r
    =>  Sem (FSRead ': r) a
    ->  Sem r a
fsReadToReadFile = interpret \case
    ReadFileBS path ->
        toSomeBase path >>= \case
            Abs path -> readFile path
            Rel path -> readFile path
    ReadFileUtf8 path ->
        toSomeBase path >>= \case
            Abs path -> readFile path
            Rel path -> readFile path

fsWriteToScopedWrite
    ::  ∀handle0 handle1 handle2 handle3 r a
    .   Members
            '[  OverwriteFile Abs BS.ByteString
            ,   OverwriteFile Rel BS.ByteString
            ,   OverwriteFile Abs T.Text
            ,   OverwriteFile Rel T.Text
            ,   Error PathException
            ] r
    =>  Sem (FSWrite ': r) a
    ->  Sem r a
fsWriteToScopedWrite = interpret \case
    WriteFileBS path content ->
        toSomeBase path >>= \case
            Abs path -> overwriteFile path content
            Rel path -> overwriteFile path content
    WriteFileUtf8 path content ->
        toSomeBase path >>= \case
            Abs path -> overwriteFile path content
            Rel path -> overwriteFile path content

toSomeBase :: Members '[Error PathException] r => Path b File -> Sem r (SomeBase File)
toSomeBase = toFilePath >>> parseSomeFile
-- Perhaps, an error can't occur definitely so the error effect can be removed...