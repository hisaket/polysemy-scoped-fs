{-
This module contains the modified source code from polysemy-1.7.1.0 package
under 'BSD-3-Clause'. So that part is only licensed under
'AGPL-3.0-or-later AND BSD-3-Clause', other part is licensed under
'AGPL-3.0-or-later'.
-}

-- -----BEGIN AGPL-3.0-or-later PART-----
-- Copyright (C) 2022 Hisaket VioletRed

{-|
License     : See the header comment of the source file of this module.
Maintainer  : hisaket@outlook.jp
Stability   : experimental
-}

module Polysemy.FS.Scoped.Internal where

import Polysemy
    ( Sem, embed, Embed, InterpretersFor, Members, rewrite )
import Polysemy.Internal.Kind ( Append )
import Polysemy.Bundle ( runBundle, Bundle (Bundle) )
import Polysemy.Internal.Sing
    ( SList (SEnd, SCons), KnownList (singList) )
import Polysemy.Internal.Union
    ( decomp
    , extendMembershipLeft
    , hoist
    , membership
    , ElemOf
    , Union(Union)
    , Weaving(Weaving)
    )
import Polysemy.Internal
    ( Sem, Append, embed, hoistSem, Embed, InterpretersFor, Members )
import Polysemy.Resource ( bracket, Resource )
import qualified System.IO as IO
import Polysemy.Path ( toFilePath, Path, File )
import Polysemy.Scoped.Parameterized ( ScopedP, runScopedP )
import Polysemy.FS.Scoped.Internal.MembersProof
    ( MembersProof (MembersProof), membersProof, membersProofId )
import Polysemy.Scoped.Parameterized.Nonleaky
    ( NonleakyScopedP, runNonleakyScopedP )

newtype ScopedFile mode es b handle m a =
    ScopedFile { unScopedFile :: NonleakyScopedP (Path b File) handle (Bundle es) m a }

scopedFileToIO
    ::  (Members '[Embed IO, Resource] r, KnownList es)
    =>  (FilePath -> IO.IOMode -> IO IO.Handle)
    ->  IO.IOMode
    ->  (IO.Handle -> InterpretersFor es r)
    ->  Sem (ScopedFile mode es b IO.Handle ': r) a
    ->  Sem r a
scopedFileToIO openFile mode =
    runScopedFile
        (\path -> bracket (embed $ openFile (toFilePath path) mode) (embed . IO.hClose))

runScopedFile
    ::  KnownList es
    =>  (∀x. Path b File -> (handle -> Sem r x) -> Sem r x)
    ->  (handle -> InterpretersFor es r)
    ->  Sem (ScopedFile mode es b handle ': r) a
    ->  Sem r a
runScopedFile acquire interpret m =
    runNonleakyScopedP acquire (\h -> interpret h . runBundle) $ rewrite unScopedFile m


seekToBegin :: IO.Handle -> IO ()
seekToBegin h = IO.hSeek h IO.AbsoluteSeek 0

seekToEnd :: IO.Handle -> IO ()
seekToEnd h = IO.hSeek h IO.SeekFromEnd 0



sendBundle_
    ::  ∀es r a
    .   KnownList es
    =>  Sem (Append es (Bundle es ': r)) a
    ->  Sem (Bundle es ': r) a
sendBundle_ = sendBundleSListUsingProof es $ membersProofId es
    where es = singList @es

sendBundleSList
    ::  Members l es
    =>  SList l
    ->  Sem (Append l (Bundle es ': r)) a
    ->  Sem (Bundle es ': r) a
sendBundleSList l = sendBundleSListUsingProof l $ membersProof l

sendBundleSListUsingProof
    ::  ∀l es r a
    .   SList l
    ->  MembersProof l es
    ->  Sem (Append l (Bundle es ': r)) a
    ->  Sem (Bundle es ': r) a
sendBundleSListUsingProof = \case
    SEnd -> const id
    SCons l ->
        \(MembersProof pr prs) ->
              sendBundleSListUsingProof l prs
            . sendBundleUsing pr
                (extendMembershipLeft @_ @(Bundle es ': r) l membership)

-- -----END AGPL-3.0-or-later PART-----

-- -----BEGIN AGPL-3.0-or-later AND BSD-3-Clause PART-----
{-
Copyright Sandy Maguire (c) 2019
Copyright Hisaket VioletRed (c) 2022

All rights reserved.
-}

-- This function is modified version from 'Polysemy.Bundle.sendBundle' in polysemy-1.7.1.0 package.
sendBundleUsing :: ElemOf e r' -> ElemOf (Bundle r') r -> Sem (e ': r) a -> Sem r a
sendBundleUsing prE prBundle =
    hoistSem $ \u -> case decomp u of
        Right (Weaving e s wv ex ins) ->
            Union prBundle $
                Weaving (Bundle prE e) s (sendBundleUsing prE prBundle . wv) ex ins
        Left g -> hoist (sendBundleUsing prE prBundle) g
{-# INLINE sendBundleUsing #-}

{-
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.

    * Neither the name of Sandy Maguire nor the names of other
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-}
-- -----END AGPL-3.0-or-later AND BSD-3-Clause PART-----