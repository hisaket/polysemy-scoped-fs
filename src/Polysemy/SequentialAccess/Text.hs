{-|
Description : Sequential access for variable-width encoded text data
Copyright   : (c) Hisaket VioletRed, 2022
License     : AGPL-3.0-or-later
Maintainer  : hisaket@outlook.jp
Stability   : experimental
Portability : POSIX

This module is for sequential access to variable-width encoded text data.

In sequential access for variable-width encoded (e.g. UTF-8) text data, It is
allowed to read data per line instead of by arbitrary size.
And, seek operation is only allowed to both ends of a file.

This restriction of safety assumes no writing from others while opening a file.
The behavior is undefined if the assumption is not satisfied. (An encoding corruption occurs typically.)
-}

module Polysemy.SequentialAccess.Text where

import qualified Polysemy.SequentialAccess as SA
import Data.Text ( Text )

type GetPosition = SA.GetPosition SA.TriPosition
type Seek = SA.Seek SA.Ends
type ReadLine = SA.Read Line Text
type ReadToEnd = SA.Read SA.ToEnd Text
type Extend = SA.Extend Text
type Append = SA.Append Text
type Clear = SA.Resize SA.NullSize

{- |A singleton that represents the size from the current position to the next
   line break.
-}
data Line = Line deriving (Eq, Ord, Show)

type Cursor = '[GetPosition, Seek]
