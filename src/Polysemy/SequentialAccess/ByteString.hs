{-|
Description : Sequential access for binary data
Copyright   : (c) Hisaket VioletRed, 2022
License     : AGPL-3.0-or-later
Maintainer  : hisaket@outlook.jp
Stability   : experimental
Portability : POSIX

This module is for sequential access to binary data.

In sequential access for binary data, it is allowed to read data by arbitrary
size, and overwrite to, and seek to an arbitrary position.
-}

module Polysemy.SequentialAccess.ByteString where

import qualified Polysemy.SequentialAccess as SA
import Data.ByteString ( ByteString )
import Numeric.Natural ( Natural )

type GetPosition = SA.GetPosition Natural
type ReadBytes = SA.Read Natural ByteString
type ReadToEnd = SA.Read SA.ToEnd ByteString
type Overwrite = SA.Overwrite ByteString
type Append = SA.Append ByteString
type Resize = SA.Resize Natural

type Seek = '[SA.Seek (SA.Absolute Natural), SA.Seek (SA.Relative Integer), SA.Seek (SA.FromEnd Integer)]
type Cursor = GetPosition ': Seek