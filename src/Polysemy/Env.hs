{-|
Copyright   : (c) Hisaket VioletRed, 2022
License     : AGPL-3.0-or-later
Maintainer  : hisaket@outlook.jp
Stability   : experimental
-}

module Polysemy.Env where

import Polysemy ( Member, Sem, interpret, makeSem )
import Polysemy.Input ( Input (Input) )

-- | Variant of 'Input' effect that has no side effects.
data Env i m a where
    Env :: Env i m i
makeSem ''Env

{- |Transforms an 'Input' effect into an `Env` effect.
    Note that "askToInput" of inverted version is not provided because an 'Env' effect is specialized version of an 'Input' effect.
-}
inputToEnv :: Member (Env i) r => Sem (Input i ': r) a -> Sem r a
inputToEnv = interpret \Input -> env

-- | Runs an 'Env' effect.
runEnvConst :: i -> Sem (Env i ': r) a -> Sem r a
runEnvConst i = interpret \Env -> pure i